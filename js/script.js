console.log('I am external');


let num = 10;
console.log(num)

let name = 'Juan'
console.log(name)

myVariable = "New Initialized Value"
console.log(myVariable)


myVar= "newvar"
console.log(myVar)


let num2 = 5;
console.log(num2);

num2 = 5+3;
console.log(num2);



let brand = 'toyota', model = 'vios', type = 'sedan';
console.log(brand);
console.log(model);
console.log(type);


let country = 'Philippines';
let province = "Manila";

console.log(typeof(country));
console.log(province);

let firstName = "John";
let lastName = "Javellana";

console.log(firstName);
console.log(lastName);

console.log(firstName + " " + lastName)

let fullName = firstName + " " + lastName

console.log(fullName)


let word1 = 'is';
let word2 = 'bootcamper';
let word3 = 'School';
let word4 = 'of';
let word5 = 'Zuitt Coding';
let word6 = 'a';
let word7 = 'Institute';
let word8 = 'Bootcamp';
let space = ' ';


let sentence = fullName + space + word1 + space + word6 + space + word2 + space + word4 + space + word5 + space + word8

console.log(sentence)

let sentenceLiteral = `${fullName} ${word1} ${word6} ${word2} ${word4} ${word5} ${word8} ${word7}`

console.log(sentenceLiteral) 

let numString1 = '7'; /*with the use of ' ', resulting to a string (concantenation)*/
let numString2= '6';

let numA = 7; /*WITHOUT the use of ' ', considered as number or as a mathematical operation*/
let numB = 5;

console.log(numString1 + numString2);
console.log(numA + numB);

let num3 = 5.5;
let num4 = .5;


console.log(numA +num3)
console.log(num3 +num4)

/*forced coercion


	string + num = concatenation
*/





/* parseInt () - changes a numeric string into a proper number or mathematical computation

*/

console.log(num4 + parseInt(numString1)) 

let sum = numA + parseInt(numString1)
console.log(sum);


console.log(num3 - numA);
console.log(num3 - num4);
console.log(numString1 - numB); /*numString1 has been forced coerced because of a mathematical operation like substraction*/

console.log(numString2 - numB);
console.log(numString1 - numString2);

let sample = 'thonie';
console.log(sample - numString1);

/*multiplication*/

console.log(numA * numB);
console.log(numString1 * numA);
console.log(numString1 * numString2); /*also forced coerced due to a mathematical operation*/

let product = numA * numB;
let product2 = numString1 * numA;
let product3 = numString1 * numString2;

console.log(product/numB);
console.log(product2 / 5);
console.log(numString2 / numString1);

console.log(product * 0);
console.log(product3 / 0);


/*modulo % = result is the remainder of division operation*/
console.log(product2 % numB);
console.log(numString1 % numB);

let isAdmin = true;
let isMarried = false;
let isGwapo = true;

/*concatenate string and boolean*/
console.log('Is Rommel handsome?' + " " + isGwapo);

/*example of array*/

let array = ['Goku', 'Piccolo', 'Gohan', 'Vegeta']
console.log(array);

/*example of objects*/

let hero = { 
	heroName: 'One Punch Man',
	isActive: true,
	salary: 500,
	realName: 'Saitama',
	height: 200
}
console.log(hero)



let arrayMCR = ['Gerard Way', 'Ray Toro', 'Mikey Way', 'Frank Iero']
console.log(arrayMCR);

let hero2 = {
  firstName: 'John',
  LastName: 'Javellana',
  isDeveloper: 'true',
  hasPortfolio: 'true',
  age: '30'
};

console.log(hero2);

let sampleNull = null; /*null, you defined it as null*/

let sampleUndefined; /*undefined, did not define anything*/
console.log(sampleNull);
console.log(sampleUndefined);


let foundResult = null;
let person2 = {
	name: 'Juan',
	isAdmin: true,
	age: 35
}

console.log(person2.isAdmin);


function greet() {
	console.log('Hello, my name is Saitama.');
}

/*invoke = to call a function*/

greet();
greet();
greet();
greet();
greet();
greet();

/*parameter and arguments*/
function printName(name){
	console.log(`My name is ${name}`) /*backtick is used to consider the sentence as a parameter.*/

}


printName('thonie'); /*function is called (printName) then thonie is the argument*/


function displayNum(number){
	console.log(number);
}
displayNum(5000);

/*multiple paramenters and arguments*/

function displayFullName(firstName, lastName, age){
	console.log(`${firstName}, ${lastName}, ${age}`)
}

displayFullName('Juan', 'Masipag', 29)
	

function createName(firstName, lastName){
	return `${firstName} ${lastName}`
	console.log("I will no longer run because the function's value has bee returned")
}

let fullName1 = createName('Tom', 'Cruise');
console.log(fullName1);






/*mini activity*/
function printAddition(Addition1){
	console.log(`${Addition1}`); 
}
printAddition(2 + 3);


function printSubtraction(Subtraction1){
	console.log(`${Subtraction1}`);
}
printSubtraction(5 - 2);



function multiplication(multiply1){
	return `${multiply1}`
	console.log("return this one")
}

let multiplyResult = multiplication(5 * 2);
console.log(multiplyResult);